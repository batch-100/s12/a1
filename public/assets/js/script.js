// alert("heelo") //tocheck if connected si script
// Activity:
// Create functions to solve the following problems
// 1. Determine if a number is divisible by 3 or 5 or both
// if the number is divisible by 3, return "Fizz"
// if the number is divisible by 5, return "Buzz"
// if the number is divisible by 3 AND 5, return "FizzBuzz"
// if the number is not divisible by 3 and 5, return "Pop"

// Test cases:
// 5 -> Buzz
// 30 -> FizzBuzz
// 27 -> Fizz
// 17 -> Pop
// 
function divisibilityTest (number){
    if (number % 15 == 0){
        return "FizzBuzz";
    }
    else if (number % 3 == 0){
        return "Fizz";
    }
    else if (number % 5 == 0){
        return "Buzz";
    }
    else{
        return "Pop";
    }
}
console.log(divisibilityTest(5)); //Buzz
console.log(divisibilityTest(30)); //divisible siya by 3 and 5 w
console.log(divisibilityTest(27));
console.log(divisibilityTest(17));


/***************************/
// 2. Make a function that gets a letter from roygbiv and it returns the corresponding color. (r -> Red, o -> Orange, etc.) Make sure that you sanitize your input

// Test cases:
// r -> Red
// G -> Green
// x -> No color
// B -> Blue
// i -> Indigo
// 
function color(letter){
    let letterCleaned = letter.toLowerCase();

    if (letter == "r"){
        return "Red";
    }
    else if (letter == "G"){
        return "Green";
    }
    else if (letter == "x"){
        return "No Color";
    }
    else if (letter == "B"){
        return "Blue";
    }
    else if (letter == "i"){
        return "Indigo";
    }
    else if (letter == "o"){
        return "Orange";
    }
    else if (letter == "y"){
        return "Yellow"
    }
    else if (letter == "v"){
        return "Violet"
    }
    else{
        return "invalid";
    }
}

console.log(color("r"));
console.log(color("G"));
console.log(color("x"));
console.log(color("B"));
console.log(color("i"));

/***************************/
// 3. (Stretch goal) Make a function that determines if a year is a leap year or not.

// Test case:
// 1900 -> Not a leap year
// 2000 -> Leap year
// 2004 -> Leap year
// 2021 -> Not a leap year

//  1 year ay 365 days.. then and leap year ay 366 days. bago mag leap year, 4 years muna na normal years. bale parang pag pinagsama, ang isang year ay magiging 365.25 days

// function leapYear(date){
//     if (){
//         return 
//     }

// }
// console.log(leapYear(1900));
// console.log(leapYear(2000));
// console.log(leapYear(2004));
// console.log(leapYear(2021));